﻿using FeedbackApp.Models;
using System.Collections.Generic;

namespace FeedbackApp.Services
{
    public interface IQuestionnaireService
    {
        Questionnaire GetQuestionnaire();
        List<Questionnaire> SaveQuestionnaire(Questionnaire questionnaire);
    }
}
