﻿using FeedbackApp.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace FeedbackApp.Services
{
    public class QuestionnaireService : IQuestionnaireService
    {
        private List<Questionnaire> lstSurvey = new List<Questionnaire>();

        public Questionnaire GetQuestionnaire()
        {
            var questions = new Questionnaire();
            using (var s = new StreamReader("questions.json"))
            {
                string json = s.ReadToEnd();
                questions = JsonConvert.DeserializeObject<Questionnaire>(json);
            }
            return questions;
        }
        
        public List<Questionnaire> SaveQuestionnaire(Questionnaire questionnaire)
        {            
            lstSurvey.Add(questionnaire);
            return lstSurvey;
            //string jsonData = JsonConvert.SerializeObject(questionnaire, Formatting.None);
            //File.WriteAllText(Environment.CurrentDirectory + "", jsonData);
        }
    }
}
