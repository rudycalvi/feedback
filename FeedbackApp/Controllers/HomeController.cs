﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using FeedbackApp.Models;
using FeedbackApp.Services;

namespace FeedbackApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IQuestionnaireService _questionnaireService;
        public HomeController(IQuestionnaireService questionnaireService)
        {
            _questionnaireService = questionnaireService;
        }
        public IActionResult Index()
        {
            var questions = _questionnaireService.GetQuestionnaire();
            return View(questions);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(Questionnaire questionnaire)
        {
            if (ModelState.IsValid)
            {
                if (questionnaire != null)
                {
                    try
                    {
                        var answers = _questionnaireService.SaveQuestionnaire(questionnaire);
                        return View("Answer", answers);
                    }
                    catch (System.Exception)
                    {
                        return RedirectToAction("Error");
                    }                                        
                }                
            }
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
