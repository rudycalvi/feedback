﻿using System.Collections.Generic;

namespace FeedbackApp.Models
{
    public class Questionnaire
    {
        public string SurveyId { get; set; }
        public string Company { get; set; }
        public string Team { get; set; }
        public string RespondentId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Guideline { get; set; }
        public string EmailAddress { get; set; }
        public List<Questions> Questions { get; set; }
    }
    public class Questions
    {
        public string Question { get; set; }
        public int QuestionNr { get; set; }
        public string QuestionType { get; set; }
        public string RadioButtonAnswerNr { get; set; }
        public string RadioButtonAnswer { get; set; }
        public List<AnswerPossibility> AnswerPossibilities { get; set; }
    }

    public class AnswerPossibility
    {
        public string Answer { get; set; }
        public string AnswerNr { get; set; }
        public bool Selected { get; set; }
    }
}
